import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  // if-else
  isFirstText: boolean = false;

// ngTemplateOutlet
  firstAstronautContext: any = { astronaut: { name: 'Yuri', surname: 'Gagarin' } };
  secondAstronautContext: any = { astronaut: { name: 'Alan', surname: 'Shepard' } };

  constructor() { }

  // if-else
  onTextButtonClick():  void {
    this.isFirstText = !this.isFirstText;
  }
}
